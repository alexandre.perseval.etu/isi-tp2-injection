# Rendu "Injection"

## Binome

Valet , Tristan, email: tristan.valet.etu@univ-lille.fr
Perseval, Alexandre, email: alexandre.perseval.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
Le mécanisme présent dans le code est le regex en javascript au niveau du formulaire HTML. Avec ce regex, le formulaire n'accepte que des lettres et des chiffres.

* Est-il efficace? Pourquoi? 
Non le mécanisme n'est pas très efficace, il est facilement contournable, par exemple :
    - on va dans l'inspecteur d'éléments du navigateur
    - on supprime la propriété *onsubmit="return validate()"* dans <*form method="post" onsubmit="return validate()"*>
    - on peut maintenant envoyer tout les caractères désirés

## Question 2

* Commande curl :

`curl 'http://localhost:8080/' --data-raw 'chaine=lettre123*#_&submit=OK'`



## Question 3

* Votre commande curl pour remplacer who :

`curl 'http://localhost:8080/' --data-raw "chaine=question 3','3.3.3.3')#&submit=OK"`


* Expliquez comment obtenir des informations sur une autre table :

De la même manière avec la commande curl, il serait possible par exemple de fournir une autre requête SQL dans les paramètres POST de la requête HTTP.
Cette deuxième requête SQL pourrait être par exemple un SELECT sur une autre table.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Pour résoudre la faille nous avons utilisé une requête paramétrée. Ainsi les injections ne sont plus possibles.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

`curl 'http://localhost:8080/' --data-raw 'chaine=</li><script>alert("Bonjour!")</script><li>&submit=OK'`

* Commande curl pour lire les cookies

`curl 'http://localhost:8080/' --data-raw 'chaine=</li><script>console.log(document.cookie)</script><li>&submit=OK'`

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Pour supprimer la faille XSS, nous avons utilisé la fonction html.escape() qui permet de rendre les variables contenant des balises HTML safe. En théorie, cette fonction d'échappement est necéssaire uniquement lors de l'affichage des variables en HTML mais pour d'avantage de sécurité, il est préférable d'en mettre aussi lors de l'insertion dans la base de données. Par exemple, dans le cas ou les données de la base serait utilisée par un autre code ou une autre application, les données seront déjà safe.


